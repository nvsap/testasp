﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CashMachine.Models
{
    public class PurchaseItems
    {
        public int Id { get; set; }
        [ForeignKey("Purchase")] public int Purchase_PurchaseId { get; set; }
        [ForeignKey("Item")] public int Item_ItemId { get; set; }
        public int Ammount { get; set; }

        public Purchase Purchase { get; set; }
        public Item Item { get; set; }
    }
}