﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CashMachine.Models
{
    public class CheckElement
    {
        public string ItemName { get; set; }
        public int Ammount { get; set; }
        public int Price { get; set; }
        public int Cost { get; set; }
        public int Total { get; set; }
        public DateTime Date { get; set; }
    }
}