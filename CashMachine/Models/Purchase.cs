﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CashMachine.Models
{
    public class Purchase
    {
        public int PurchaseId { get; set; }
        public float Total { get; set; }
        public DateTime Date { get; set; }
    }
}