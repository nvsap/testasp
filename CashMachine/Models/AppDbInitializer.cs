﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CashMachine.Models
{
    public class AppDbInitializer : DropCreateDatabaseAlways<CashContext>
    {
        protected override void Seed(CashContext db)
        {
            db.Items.Add(new Item { ItemName = "Рис", Price = 28 });
            db.Items.Add(new Item { ItemName = "Мясо", Price = 78 });
            db.Items.Add(new Item { ItemName = "Торт", Price = 158 });
            base.Seed(db);
        }
    }
}