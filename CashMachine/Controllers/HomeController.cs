﻿using CashMachine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CashMachine.Controllers
{
    public class HomeController : Controller
    {
        CashContext db = new CashContext();
        public ActionResult Index()
        {
            IEnumerable<Item> items = db.Items;
            ViewBag.Items = items;

            int todaysPurchases = db.Purchases.Where(p => p.Date.Day == DateTime.Today.Day && p.Date.Month == DateTime.Today.Month && p.Date.Year == DateTime.Today.Year).ToList().Count;
            ViewBag.Today = todaysPurchases;

            return View();
        }

        [HttpGet]
        public ActionResult AddItem()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddItem(Item item)
        {
            db.Items.Add(item);
            db.SaveChanges();
            Response.Redirect("AddItem");
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Item content = db.Items.Where(p => p.ItemId == id).FirstOrDefault();
            if (content != null)
            {
                db.Items.Remove(content);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Buy(List<int> ids, List<int> sums)
        {
            bool hasItems = false;
            int total = 0;
            Purchase newPurchase = new Purchase { Date = DateTime.Now };
            db.Purchases.Add(newPurchase);
            db.SaveChanges();

            for (int i = 0; i < sums.Count; ++i)
            {
                if (sums[i] > 0)
                {
                    hasItems = true;
                    PurchaseItems purchaseItemsItem = new PurchaseItems { Purchase = newPurchase, Item_ItemId = ids[i], Ammount = sums[i] };
                    total += (int) db.Items.Find(ids[i]).Price * sums[i];
                    db.PurchaseItems.Add(purchaseItemsItem);
                    db.SaveChanges();
                }
            }

            newPurchase.Total = total;

            if (!hasItems)
            {
                db.Purchases.Remove(newPurchase);
                db.SaveChanges();
                ViewBag.Items = db.Items;
                ViewBag.Today = db.Purchases.Where(p => p.Date.Day == DateTime.Today.Day && p.Date.Month == DateTime.Today.Month && p.Date.Year == DateTime.Today.Year).ToList().Count;

                return View("Index"); //TODO: Return same view but with error message
            }

            db.SaveChanges();

            return CheckOut(newPurchase.PurchaseId);
        }

        public ActionResult CheckOut(int purchaseId)
        {
            Purchase purchase = db.Purchases.Find(purchaseId);

            List<PurchaseItems> check = db.PurchaseItems.Where(b => b.Purchase_PurchaseId == purchaseId).ToList();
            List<CheckElement> checkElements = new List<CheckElement>();

            foreach(PurchaseItems item in check)
            {
                checkElements.Add(new CheckElement { ItemName = item.Item.ItemName, Price = (int)item.Item.Price, Ammount = item.Ammount, Cost = (int)item.Item.Price * item.Ammount, Total = (int) purchase.Total, Date = purchase.Date });
            }

            ViewBag.Items = checkElements;

            return View("Check");
        }
    }
}